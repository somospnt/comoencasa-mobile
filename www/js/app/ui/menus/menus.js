comoencasa.ui.menus = (function() {

    var codigoCasa = "bendita-vianda";
    var nombreCasa = "Bendita Vianda";

    function init() {
        comoencasa.service.menu.get(codigoCasa).done(mostrar).fail(mostrarError);
        
        $("#titulo").html(nombreCasa);
    }
    
    function mostrar(menus){
        menus.codigoCasa = codigoCasa;
        var htmlMenus = $.templates("#menuTemplate").render(menus);
        $('#menus').html(htmlMenus);
        $('#menus').listview("refresh");
    }
    
    function mostrarError(){
        
    }

    return {
        init: init
    };


})();

$(document).ready(function(){
   comoencasa.ui.menus.init(); 
});