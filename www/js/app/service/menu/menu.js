comoencasa.service.menu = (function(){
    
    function get(codigoCasa){
        return $.get('http://comoencasa.somospnt.com/api/menus/' + codigoCasa);
    }
    
    return {
        get: get
    };
})();
